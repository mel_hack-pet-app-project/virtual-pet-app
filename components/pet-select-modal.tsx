import React from "react";
import { Pressable } from "react-native";
import styled from "styled-components/native";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/Entypo";
import { defaultPets } from "../constants/Pets";

const ModalView = styled.View`
  background-color: white;
  border-radius: 25px;
  width: 100%;
  height: 80%;
  padding: 5%;
`;

const CenterView = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const PetButton = styled.Pressable`
  background-color: white;
  justify-content: center;
  align-items: center;
  border-radius: 30px;
  margin: 5%;
  padding: 0;
  width: 90%;
  flex: 1;
  elevation: 8;
  /* Elevation won't work on ios, so do shadow instead.
   * I have no idea if it works.
  */
  box-shadow: 2px 1px 4px black;
`;

const TitleText = styled.Text`
  color: black;
  font-size: 27px;
`;

const RowView = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const PetAvatar = styled.Image`
  border-radius: 200px;
  width: 80px;
  height: 80px;
`;

const XButton = ({ onPress }: any) => (
  <Pressable style={{ alignSelf: "flex-end" }} onPress={onPress}>
    <Icon name="cross" color="black" size={30} />
  </Pressable>
);

const PetChoice = ({ image, onPress }: any) => {
  return (
    <CenterView>
      <PetButton onPress={onPress}>
        <PetAvatar source={image} />
      </PetButton>
    </CenterView>
  );
};

const PetSelection = ({ visible, close, toHome }: any) => {
  return (
    <Modal
      isVisible={visible}
      animationIn="fadeInUp"
      animationOut="fadeOutDown"
      onBackButtonPress={close}
      onBackdropPress={close}
    >
      <ModalView>
        <XButton onPress={close} />
        <TitleText style={{ margin: "2%", alignSelf: "center" }}>
          Choose your pet:
        </TitleText>
        <RowView>
          <PetChoice
            image={defaultPets.dog.imgPath}
            onPress={toHome(defaultPets.dog)}
          />
          <PetChoice
            image={defaultPets.cat.imgPath}
            onPress={toHome(defaultPets.cat)}
          />
        </RowView>
        <RowView>
          <PetChoice
            image={defaultPets.panda.imgPath}
            onPress={toHome(defaultPets.panda)}
          />
          <PetChoice
            image={defaultPets.bear.imgPath}
            onPress={toHome(defaultPets.bear)}
          />
        </RowView>
        <RowView>
          <PetChoice
            image={defaultPets.monkey.imgPath}
            onPress={toHome(defaultPets.monkey)}
          />
          <PetChoice
            image={defaultPets.tiger.imgPath}
            onPress={toHome(defaultPets.tiger)}
          />
        </RowView>
        <RowView>
          <PetChoice
            image={defaultPets.lion.imgPath}
            onPress={toHome(defaultPets.lion)}
          />
          <PetChoice
            image={defaultPets.mouse.imgPath}
            onPress={toHome(defaultPets.mouse)}
          />
        </RowView>
      </ModalView>
    </Modal>
  );
};

export default PetSelection;
