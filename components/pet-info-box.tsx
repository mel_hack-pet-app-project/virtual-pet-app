import React, { useState } from "react";
import { Pressable, View, Text } from "react-native";
import styled from "styled-components/native";
import Icon from "react-native-vector-icons/Entypo";
import { PetData } from "../types";
// @ts-ignore  -  There isn't a ts file and I don't care rn.
import Slider from "react-native-smooth-slider";

const InfoView = styled.View<{ flex?: number }>`
  flex: ${({ flex }) => (flex ? flex : 1)};
  justify-content: center;
  align-items: center;
`;

const SliderBox = styled.View`
  flex: 2;
  border-radius: 30px;
  background-color: #d5d7d5;
  elevation: 10;
  justify-content: center;
  align-items: center;
  width: 92%;
  margin-bottom: 4%;
`;

const PetAvatar = styled.Image`
  border-radius: 200px;
  width: 125px;
  height: 125px;
`;

const RoundView = styled.View`
  background-color: white;
  elevation: 11;
  border-radius: 200px;
  width: 130px;
  height: 130px;
`;

const GhostView = styled.View`
  height: 100%;
  width: 100%;
  position: absolute;
  justify-content: center;
  align-items: center;
`;

const LineText = styled.Text<{ size?: string; margin?: string }>`
  font-size: ${({ size }) => (size ? size : "12px")};
  font-weight: bold;
  margin: ${({ margin }) => (margin ? margin : "0")};
`;

const Block = ({ size = 1 }: any) => <View style={{ flex: size }} />;

type FatSliderProps = {
  title: string;
  message: string;
  milestones: string[];
  pos: number;
  lColour: string;
  rColour: string;
};

const SmallText = styled.Text`
  align-self: center;
  font-size: 12px;
`;

const FatSlider = ({
  title,
  message,
  milestones,
  pos,
  lColour,
  rColour,
}: FatSliderProps) => {
  // Having l and r colours doesn't actually look good,
  // so it's disabled for now.
  return (
    <InfoView flex={2} style={{ width: "80%" }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "flex-start",
          alignSelf: "flex-start",
          marginBottom: "2%",
        }}
      >
        <LineText
          size="22px"
          margin="0 3% 2% 0"
          style={{ alignSelf: "flex-start" }}
        >
          {title}
        </LineText>
        <SmallText>{message}</SmallText>
      </View>
      <Slider
        disabled
        value={pos}
        style={{ width: "100%", marginBottom: "3%" }}
        useNativeDriver={true}
        minimumTrackTintColor={lColour}
        maximumTrackTintColor={lColour}
        trackStyle={trackStyle}
        thumbStyle={thumbStyle}
      />
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          alignSelf: "center",
        }}
      >
        {milestones.map((val: string, i: number) => (
          <LineText margin="0 0 0 26px" key={i} style={{ flex: 1 }}>
            {val}
          </LineText>
        ))}
      </View>
    </InfoView>
  );
};
const trackStyle = {
  height: 36,
  borderRadius: 20,
};

const thumbStyle = {
  width: 49,
  height: 49,
  borderRadius: 40,
  backgroundColor: "white",
  borderColor: "lightgrey",
  borderWidth: 3,
  elevation: 3,
};

// View for displaying data about the pet in the home area.
const PetInfoView = ({ name, species, imgPath, bDay }: PetData) => {
  return (
    <InfoView>
      <InfoView>
        <LineText size="30px" margin="5% 0 3% 0">
          Welcome back Friend!
        </LineText>
        <LineText size="43px" margin="0 0 15% 0">
          {name}
        </LineText>
      </InfoView>
      <SliderBox>
        <Block size={1} />
        <FatSlider
          title="Cheer"
          message="I'm happy when you're helping the world!"
          milestones={[":(", ":|", ":)", ":D"]}
          pos={0.1 + 0.2 * 2}
          rColour="#FD0961"
          lColour="#F373A1"
        />
        <FatSlider
          title="Time"
          message="How long have we been buddies now?"
          milestones={["1Y", "2Y", "3Y", "4Y", "?"]}
          pos={0.1 + 0.25}
          rColour="#05FA2C"
          lColour="#91FC8F"
        />
        <FatSlider
          title="Battery"
          message="Battery care means we can hang out more!"
          milestones={["", ">|", "", "", "", "|<", ""]}
          pos={0.1 + 0.6}
          rColour="#05FAEB"
          lColour="#ACEBFF"
        />
        <InfoView>
          <LineText>Saving the planet, just a little!</LineText>
        </InfoView>
      </SliderBox>
      <GhostView>
        {/* Add these empty views to put the image just on the edge */}
        <Block size={2} />
        <RoundView>
          <PetAvatar source={imgPath} />
        </RoundView>
        <Block size={5} />
      </GhostView>
    </InfoView>
  );
};

export default PetInfoView;
