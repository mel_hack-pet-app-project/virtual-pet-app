import AsyncStorage from "@react-native-async-storage/async-storage";
import { StatusBar } from "expo-status-bar";
import React, { useCallback, useEffect, useState } from "react";
import { Button, Text, View } from "react-native";
import { NavigationActions } from "react-navigation";
import styled from "styled-components/native";
import { STORAGE_KEY } from "../constants/Keys";
import { PetData, RootStackParamList } from "../types";
import { StackNavigationProp } from "@react-navigation/stack";
import { useFocusEffect } from "@react-navigation/native";
import PetInfoView from "../components/pet-info-box";
import { defaultPet } from "../constants/Defaults";

const HomeView = styled.View`
  flex: 1;
  background-color: white;
  align-items: center;
  justify-content: center;
`;

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList, "Home">;

const Home = ({
  route,
}: {
  route: any;
  navigation: HomeScreenNavigationProp;
}) => {
  const { pet } = route.params;

  const [petData, setPetData] = useState<PetData>(pet);

  const getStoredData = async () => {
    try {
      const data = await AsyncStorage.getItem(STORAGE_KEY);
      if (data !== null) {
        setPetData(JSON.parse(data));
      }
    } catch (_: any) {
      alert("Storage retrieval failed.");
    }
  };

  useFocusEffect(() => {
    getStoredData();
    return () => {};
  });

  return <PetInfoView {...petData} />;
};

export default Home;
