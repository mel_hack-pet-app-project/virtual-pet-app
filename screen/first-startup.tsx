import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { Image, View } from "react-native";
import styled from "styled-components/native";
import PetSelection from "../components/pet-select-modal";
import { defaultPets } from "../constants/Pets";
import { PetData } from "../types";
/* import { NavigationActions } from "react-navigation"; */

const FullScreenView = styled.View`
  flex: 1;
  background-color: #ff4b2a;
  align-items: center;
  justify-content: center;
`;

const ChooseButton = styled.Pressable<{ color: string }>`
  background-color: ${(props) => props.color};
  justify-content: center;
  align-items: center;
  border-radius: 30px;
  padding: 15px;
  width: 220px;
  elevation: 2;
  /* Elevation won't work on ios, so do shadow instead.
   * I have no idea if it works.
  */
  box-shadow: 2px 1px 4px black;
`;

const ButtonText = styled.Text`
  font-size: 19px;
  color: #ff4b2a;
`;

const BigText = styled.Text`
  color: white;
  font-size: 40px;
  margin-bottom: 10px;
`;

const PetImage = styled(Image)`
  height: 120px;
  width: 120px;
  flex: 1;
`;

const FirstStartup = ({ navigation }: any) => {
  const [modalSelVisible, setModalSelVisible] = useState(false);

  const toggleSelection = () => setModalSelVisible(!modalSelVisible);

  const goToHome = (pet: PetData) => () => {
    toggleSelection();
    navigation.navigate("Home", { pet: pet });
  };

  return (
    <FullScreenView>
      {/* We want the status bar out of view while setting up. */}
      <StatusBar backgroundColor="transparent" translucent={true} />
      <PetSelection
        visible={modalSelVisible}
        close={toggleSelection}
        toHome={goToHome}
      />
      <View style={{ flex: 3 }} />
      <View style={{ flex: 3 }}>
        <BigText>Hi There!</BigText>
        <BigText>From Me,</BigText>
        <BigText>Your Virtual Pet.</BigText>
      </View>
      <View style={{ flex: 4 }}>
        <PetImage source={require("../assets/pawprint-cut-out.png")} />
      </View>
      <View style={{ flex: 3 }}>
        <ChooseButton
          color="white"
          onPress={() => {
            setModalSelVisible(true);
          }}
        >
          <ButtonText>Choose Pet</ButtonText>
        </ChooseButton>
      </View>
    </FullScreenView>
  );
};

export default FirstStartup;
