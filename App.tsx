import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { STORAGE_KEY } from "./constants/Keys";
import FirstStartup from "./screen/first-startup";
import Home from "./screen/home";
import { PetData, RootStackParamList } from "./types";

const Stack = createNativeStackNavigator<RootStackParamList>();

export default function App() {
  const [isFirstTime, setIsFirstTime] = useState(true);

  // Ask the local storage if this is the first time running the app.
  // If so, the user needs to select a pet and get set up!
  const getStoredData = async () => {
    try {
      const data = await AsyncStorage.getItem(STORAGE_KEY);
      // If nothing there, the app has never been run.
      setIsFirstTime(data === null);
    } catch (_: any) {
      alert("Storage retrieval failed.");
    }
  };

  useEffect(() => {
    getStoredData();
  }, []);

  return (
    // Wraps around the whole app to manage navigation history.
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{ headerShown: false }}
        initialRouteName={isFirstTime ? "Initialise" : "Home"}
      >
        <Stack.Screen name="Initialise" component={FirstStartup} />
        <Stack.Screen name="Home" component={Home} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
