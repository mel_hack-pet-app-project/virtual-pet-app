/**
 * Use this file for storing types for access across the project.
 * Mainly for Prop types, so that they can be accessed by the component
 * and also the caller.
 */

import { ImageSourcePropType } from "react-native";

export type Pets = {
  dog: PetData;
  cat: PetData;
  panda: PetData;
  bear: PetData;
  monkey: PetData;
  tiger: PetData;
  lion: PetData;
  mouse: PetData;
};

export type PetData = {
  name: string;
  species: string;
  imgPath: ImageSourcePropType;
  bDay: Date;
};

export type RootStackParamList = {
  Home: undefined;
  Initialise: undefined;
};
