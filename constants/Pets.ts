import { Pets } from "../types";

export const defaultPets: Pets = {
  dog: {
    name: "Klee",
    species: "Dog",
    imgPath: require("../assets/dog.jpeg"),
    bDay: new Date(),
  },
  cat: {
    name: "Ginger",
    species: "Cat",
    imgPath: require("../assets/cat.jpeg"),
    bDay: new Date(),
  },
  panda: {
    name: "Loaf",
    species: "Panda",
    imgPath: require("../assets/panda.jpeg"),
    bDay: new Date(),
  },
  bear: {
    name: "Claws",
    species: "Bear",
    imgPath: require("../assets/bear.jpeg"),
    bDay: new Date(),
  },
  monkey: {
    name: "Clip",
    species: "Monkey",
    imgPath: require("../assets/monkey.jpeg"),
    bDay: new Date(),
  },
  tiger: {
    name: "Slick",
    species: "Tiger",
    imgPath: require("../assets/tiger.jpeg"),
    bDay: new Date(),
  },
  lion: {
    name: "Avery",
    species: "Lion",
    imgPath: require("../assets/lion.jpeg"),
    bDay: new Date(),
  },
  mouse: {
    name: "Clicks",
    species: "Mouse",
    imgPath: require("../assets/mouse.jpeg"),
    bDay: new Date(),
  },
};
