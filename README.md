# Eco Pet App

![Image of the app screens](./assets/full-display.png)

## Overview

We're making an app that lets users raise a pet on their phones, but it only grows in real time. The aim is to encourage users to keep their phones longer, hopefully 4+ years, to minimise e-waste. Ideally it shouldn't be possible to transfer the app to a new phone without corrupting the data.

The current iteration of the app displays data about the pet's health, which is tied to several environmentally-conscious metrics.

See `releases/eco-pet-app.apk` to try out the application.

_Note: App is incomplete. This project was created for the 2021 Melbourne Hack and is no longer being actively developed._

## Setup

### Development

1. Install [Expo](https://docs.expo.dev/) on your machine, either through your package manager or by running the following in your terminal.

```
npm install -g expo-cli
```

2. If you don't have npm (via nodejs) installed, follow the instructions [here](https://nodejs.org/en/) or, again, do so through your package manager.

3. Clone this repository, enter, and start the project:

```
git clone "https://gitlab.com/mel_hack-pet-app-project/virtual-pet-app.git"
cd virtual-pet-app
expo start
```

Alternatively, use `npm start` or `yarn start` - both just run `expo start` anyway.

4. Expo should open a locally hosted web-page where you can then test the app, either in a web-page or on your phone by using the 'expo-go' app (and scanning the QR code). Saved changes to any file in the repository will cause the app to refresh.

### Troubleshooting

- Ensure that your phone and computer are both connected to the same wifi.
- Packages are managed by `yarn` by default, so don't use `npm` to actually install any packages.
